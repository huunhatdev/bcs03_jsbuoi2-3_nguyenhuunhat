/**
 * input: 23
 *
 * steps:
 *      tạo biến nhập usd
 *      tạo biến giá trị tỉ giá: 23500 vnd
 *      tạo biến giá tiền sau quy đổi
 *      tính giá tiền sau quy đổi
 *
 * output:
 */
function ex3() {
  var usd = document.getElementById("usd").value;
  var tiGia = 23500;
  var usdToVnd = null;
  usdToVnd = tiGia * usd;

  document.getElementById("result3").innerHTML = `${usd}usd => ${usdToVnd}vnđ`;
}
