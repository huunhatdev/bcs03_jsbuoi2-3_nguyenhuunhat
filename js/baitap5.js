/**
 * input: 69
 *
 * steps:
 *      tạo biến nhập số
 *      tạo biến đơn vị, chục
 *      tạo biến tính tổng 2 kí số
 *      tính tổng 2 kí số = chục + đơn vị
 *
 * output: 15
 *
 */

function ex5() {
  var number = document.getElementById("numEx5").value * 1;
  var donVi = number % 10;
  var chuc = (number - donVi) / 10;

  var sum = donVi + chuc;

  document.getElementById("result5").innerHTML = `Tổng 2 kí số: ${sum}`;
}
