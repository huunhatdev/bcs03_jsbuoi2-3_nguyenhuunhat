/**
 * input: 10 20
 *
 * steps:
 *      tạo biến 2 cạnh hình chữ nhật
 *      tạo biến chu vi và diện tích hcn
 *      tính chu vi hình chữ nhật = (dài + rộng) *2
 *      tính diện tích hình chữ nhật = dài * rộng
 *
 * output: chu vi 60, dien tich 200
 *
 */
function ex4() {
  var lenght = document.getElementById("lenght").value * 1;
  var width = document.getElementById("width").value * 1;
  var perimeter = null;
  var acreage = null;

  perimeter = (lenght + width) * 2;
  acreage = lenght * width;

  document.getElementById(
    "result4"
  ).innerHTML = `Chu vi: ${perimeter} - Diện tích: ${acreage}`;
}
