/**
 * input: nhập số ngày làm: 20
 *
 * steps:
 *  tạo biến số ngày làm
 *  tạo biến giá trị tiền lương 1 ngày: 100000
 *  tạo biến tiền lương
 *  tính lương: lương 1 ngày * số ngày làm
 *
 * output: tiền lương
 */
function ex1() {
  var numDay = document.getElementById("numDay").value;
  var money = 100000;
  var output = null;

  output = numDay * money;

  document.getElementById(
    "result1"
  ).innerHTML = `Với ${numDay} bạn nhận được: ${output}vnđ`;
}
